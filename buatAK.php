<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style2.css">
    <title>Pembuatan Akta Kelahiran</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<script type="text/javascript">
    function runPopup(){
        if(window.confirm("SILAHKAN ISI DENGAN JUJUR DAN TELITI!!!!!")) {
            window.alert("SILAHKAN DIISI!!!");
        }
    };
</script>
<body class="badan">
    <nav class="navbar navbar-inverse">
    <div class="container-fluid">
     <div class="navbar-header">
    <a href="index.php" class="navbar-brand">Pembuatan Dokumen Sipil Online</a>
   </div>
   <ul class="nav navbar-nav">
    <li><a href="index.php">Home</a></li>
    <li><a href="cs.php">Customer Services</a></li>
   </ul>
  </div>
 </nav>
<button onclick="runPopup()">Click Me</button>
  <script>  
     if(window.confirm("SILAHKAN ISI DENGAN JUJUR DAN TELITI!!!!")) {
        window.alert("SILAHKAN DIISI!!!");
     }
  </script>
    <form action="buatAK.php" method="POST">
        <fieldset>
        <legend>Akta Kelahiran</legend>
        <table border="2">
            <tr>
                <td><p><label>Nama Kelahiran Anak : </label> <input type="text" name="nama_anak" placeholder="Nama Kelahiran Anak..." /></p></td>
            </tr>
            <tr>
                <td><p><label>Tanggal dan Waktu Kelahiran Anak : </label><input type="text" name="lahir_anak" placeholder="Tanggal dan Waktu Kelahiran Anak..." /></p></td>
            </tr>
            <tr>
                <td><p>
                    <label>Jenis Kelamin Anak : </label>
                    <label><input type="radio" name="jenis_kelamin" value="LAKI-LAKI" /> LAKI-LAKI</label>
                    <label><input type="radio" name="jenis_kelamin" value="PERREMPUAN" /> PEREMPUAN</label>
                </p></td>
            </tr>
            <tr>
                <td>><p><label>Tempat Kelahiran Anak : </label><input type="text" name="tempat_anak" placeholder="Tempat Kelahiran Anak..." /></p></td>
            </tr>
            <tr><
                <td><p><label>Nama Kedua Orang Tua Anak : </label><input type="text" name="ayah_anak" placeholder="AYAH" /><input type="text" name="ibu_anak" placeholder="IBU"></p></td>
            </tr>
            <tr>
                <td><p><label>Pekerjaan Kedua Orang Tua Anak : </label><input type="text" name="kerjaayah" placeholder="AYAH" /><input type="text" name="kerjaibu" placeholder="IBU"></p></td>
            </tr>
            <tr>
                <td><p><label>Berat Anak : </label><input type="text" name="berat_anak" placeholder="Berat Anak..." /></p></td>
            </tr>
            <tr>
                <td><p><label>Tinggi Anak : </label><input type="text" name="tinggi_anak" placeholder="Tinggi Anak..." /></p></td>
            </tr>
            <tr>
                <td><p><label>Tanggal Pencatatan Pendaftaran Kelahiran : </label><input type="date" name="catat_lahir" placeholder="Tanggal Pencatatan Pendaftaran Kelahiran.." /></p></td>
            </tr>
    </table>
        <p>
            <input type="submit" name="submit" value="Buat Akta Kelahiran" />
        </p>
        </fieldset>
    </form>
    <?php 
        if(isset($_POST['submit'])){
            $nama_anak = $_POST['nama_anak'];
            $lahir_anak = $_POST['lahir_anak'];
            $jenis_kelamin = $_POST['jenis_kelamin'];
            $tempat_anak = $_POST['tempat_anak'];
            $ayah_anak = $_POST['ayah_anak'];
            $ibu_anak = $_POST['ibu_anak'];
            $kerjaayah = $_POST['kerjaayah'];
            $kerjaibu = $_POST['kerjaibu'];
            $berat_anak = $_POST['berat_anak'];
            $tinggi_anak = $_POST['tinggi_anak'];
            $catat_lahir = $_POST['catat_lahir'];
            echo "<legend>Hasil Akta Kelahiran</legend>";
            echo "Nama Kelahiran Anak : "; echo $nama_anak;
            echo "<br/>";
            echo "Tanggal dan Waktu Kelahiran Anak : "; echo $lahir_anak;
            echo "<br/>";
            echo "Jenis Kelamin Anak : "; echo $jenis_kelamin;
            echo "<br/>";
            echo "Tempat Kelahiran Anak : "; echo $tempat_anak;
            echo "<br/>";
            echo "Nama Kedua Orang Tua Anak : "; echo "<br/>";
            echo "Nama Ayah : "; echo $ayah_anak; echo "<br/>";
            echo "Nama Ibu : "; echo $ibu_anak; 
            echo "<br/>";
            echo "Pekerjaan Kedua Orang Tua Anak : "; echo "<br/>";
            echo "Pekerjaan Ayah : "; echo $kerjaayah; echo "<br/>";
            echo "Pekerjaan Ibu : "; echo $kerjaibu;
            echo "<br/>";
            echo "Berat Anak : "; echo $berat_anak;
            echo "<br/>";
            echo "Tinggi Anak : "; echo $tinggi_anak;
            echo "<br/>";
            echo "Tanggal Pencatatan Pendaftaran Kelahiran : "; echo $catat_lahir;
            echo "<br>";
            echo "Silahkan Upload File : ";
            echo "<form action=buatKTP.php method=post enctype=multipart/form-data>"; 
                echo "Upload Surat Keterangan Kelahiran dari kelurahan : ";
                echo "<input type=file name=fileup>";
                echo "Upload Surat Keterangan Kelahiran dari Saksi : ";
                echo "<input type=file name=fileup>";
                echo "Upload Buku/Surat/Akta Nikah : ";
                echo "<input type=file name=fileup>";
                echo "Upload KTP : "; echo "<br>";
                echo "AYAH = <input type=file name=fileup>";
                echo "IBU = <input type=file name=fileup>";
                echo "Upload KK : ";
                echo "<input type=file name=fileup>";
            echo "</form>";
    }
     ?>
    <h4>Silahkan klik di bawah untuk pembayaran</h4>
    <button><a href="pembayaran.php">PEMBAYARAN</a></button>
</body>
<center><footer>copyright</footer></center>
<center><footer>Muhammad Daffiano Rahmatullah</footer></center>
<center><footer>1900018081</footer></center>
<center><footer>est 2021</footer></center>
</html>
