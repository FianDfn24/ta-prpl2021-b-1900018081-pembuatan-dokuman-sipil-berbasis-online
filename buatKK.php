<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="style2.css">
    <title>Pembuatan Kartu Keluarga</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<script type="text/javascript">
    function runPopup(){
        if(window.confirm("SILAHKAN ISI DENGAN JUJUR DAN TELITI!!!!!")) {
            window.alert("SILAHKAN DIISI!!!");
        }
    };
</script>
<body class="badan">
	<nav class="navbar navbar-inverse">
    <div class="container-fluid">
     <div class="navbar-header">
    <a href="index.php" class="navbar-brand">Pembuatan Dokumen Sipil Online</a>
   </div>
   <ul class="nav navbar-nav">
    <li><a href="index.php">Home</a></li>
    <li><a href="cs.php">Customer Services</a></li>
   </ul>
  </div>
 </nav>
<button onclick="runPopup()">Click Me</button>
  <script>  
     if(window.confirm("SILAHKAN ISI DENGAN JUJUR DAN TELITI!!!!")) {
        window.alert("SILAHKAN DIISI!!!");
     }
  </script>
 <form action="buatKK.php" method="POST">
        <legend>KARTU KELUARGA</legend>
        <table border="5">
        <tr border="5">
            <td><p><label>Nama Lengkap Keluarga : </label></p>
                <p><input type="text" name="kepal" placeholder="Kepala Keluarga..."></p>
                <p><input type="text" name="angkel1" placeholder="Anggota Keluarga 1..."></p>
                <p><input type="text" name="angkel2" placeholder="Anggota Keluarga 2..."></p>
                <p><input type="text" name="angkel3" placeholder="Anggota Keluarga 3..."></p>
                <p><input type="text" name="angkel4" placeholder="Anggota Keluarga 4..."></p>
                <p><input type="text" name="angkel5" placeholder="Anggota Keluarga 5..."></p></td>
            <td><p><label>NIK : </label></p>
                    <p><input type="number" name="nik1" placeholder="Kepala Keluarga..."></p>
                    <p><input type="number" name="nik2" placeholder="Anggota Keluarga 1..."></p>
                    <p><input type="number" name="nik3" placeholder="Anggota Keluarga 2..."></p>
                    <p><input type="number" name="nik4" placeholder="Anggota Keluarga 3..."></p>
                    <p><input type="number" name="nik5" placeholder="Anggota Keluarga 4..."></p>
                    <p><input type="number" name="nik6" placeholder="Anggota Keluarga 5..."></p></td>
            <td><p><label>Jenis kelamin : </label>
                    <p>Kepala keluarga : </p>
                    <input type="radio" name="jkkepal" value="LAKI-LAKI" /> LAKI-LAKI
                    <input type="radio" name="jkkepal" value="PERREMPUAN" /> PEREMPUAN
                    <p>Anggota Keluarga 1 : </p>
                    <input type="radio" name="jkangkel1" value="LAKI-LAKI" /> LAKI-LAKI
                    <input type="radio" name="jkangkel1" value="PERREMPUAN" /> PEREMPUAN
                    <p>Anggota Keluarga 2 : </p>
                    <input type="radio" name="jkangkel2" value="LAKI-LAKI" /> LAKI-LAKI
                    <input type="radio" name="jkangkel2" value="PERREMPUAN" /> PEREMPUAN
                    <p>Anggota Keluarga 3 : </p>
                    <input type="radio" name="jkangkel3" value="LAKI-LAKI" /> LAKI-LAKI
                    <input type="radio" name="jkangkel3" value="PERREMPUAN" /> PEREMPUAN
                    <p>Anggota Keluarga 4 : </p>
                    <input type="radio" name="jkangkel4" value="LAKI-LAKI" /> LAKI-LAKI
                    <input type="radio" name="jkangkel4" value="PERREMPUAN" /> PEREMPUAN
                    <p>Anggota Keluarga 5 : </p>
                    <input type="radio" name="jkangkel5" value="LAKI-LAKI" /> LAKI-LAKI
                    <input type="radio" name="jkangkel5" value="PERREMPUAN" /> PEREMPUAN</td>
            <td><p><label>Alamat : </label><input type="text" name="alamat" placeholder="Alamat..." /></p></td>
            <td><p><label>Tempat Lahir : </label></p>
                <p><input type="text" name="tempat1" placeholder="Kepala Keluarga..."></p>
                <p><input type="text" name="tempat2" placeholder="Anggota Keluarga 1..."></p>
                <p><input type="text" name="tempat3" placeholder="Anggota Keluarga 2..."></p>
                <p><input type="text" name="tempat4" placeholder="Anggota Keluarga 3..."></p>
                <p><input type="text" name="tempat5" placeholder="Anggota Keluarga 4..."></p>
                <p><input type="text" name="tempat6" placeholder="Anggota Keluarga 5..."></p></td>
            <td><p><label>Tanggal Lahir : </label></p>
                <p><input type="text" name="tl1" placeholder="Kepala Keluarga..."></p>
                <p><input type="text" name="tl2" placeholder="Anggota Keluarga 1..."></p>
                <p><input type="text" name="tl3" placeholder="Anggota Keluarga 2..."></p>
                <p><input type="text" name="tl4" placeholder="Anggota Keluarga 3..."></p>
                <p><input type="text" name="tl5" placeholder="Anggota Keluarga 4..."></p>
                <p><input type="text" name="tl6" placeholder="Anggota Keluarga 5..."></p></td>
        </tr>
        <tr border="5">
            <td><p><label>Agama : </label>
                <p>Kepala Keluarga :
                    <select name="agamakepal">
                        <option value=""></option>       
                        <option value="islam">Islam</option>
                        <option value="kristen">Kristen</option>
                        <option value="Khatolik">Khatolik</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                        <option value="Kong Hu Cu">Kong Hu Cu</option>
                    </select></p>
                <p>Anggota Keluarga 1 :
                    <select name="agamaangkel1">
                        <option value=""></option>       
                        <option value="islam">Islam</option>
                        <option value="kristen">Kristen</option>
                        <option value="Khatolik">Khatolik</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                        <option value="Kong Hu Cu">Kong Hu Cu</option>
                    </select></p>
                <p>Anggota Keluarga 2 :
                    <select name="agamaangkel2">
                        <option value=""></option>       
                        <option value="islam">Islam</option>
                        <option value="kristen">Kristen</option>
                        <option value="Khatolik">Khatolik</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                        <option value="Kong Hu Cu">Kong Hu Cu</option>
                    </select></p>
                <p>Anggota Keluarga 3 :
                    <select name="agamaangkel3">
                        <option value=""></option>       
                        <option value="islam">Islam</option>
                        <option value="kristen">Kristen</option>
                        <option value="Khatolik">Khatolik</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                        <option value="Kong Hu Cu">Kong Hu Cu</option>
                    </select></p>
                <p>Anggota Keluarga 4 :
                    <select name="agamaangkel4">
                        <option value=""></option>       
                        <option value="islam">Islam</option>
                        <option value="kristen">Kristen</option>
                        <option value="Khatolik">Khatolik</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                        <option value="Kong Hu Cu">Kong Hu Cu</option>
                    </select></p>
                    <p>Anggota Keluarga 5 :
                    <select name="agamaangkel5">
                        <option value=""></option>       
                        <option value="islam">Islam</option>
                        <option value="kristen">Kristen</option>
                        <option value="Khatolik">Khatolik</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                        <option value="Kong Hu Cu">Kong Hu Cu</option>
                    </select></p></td>
            <td><p><label>Pendidikan : </label></p>
                <p><input type="text" name="didik1" placeholder="Kepala Keluarga..."></p>
                <p><input type="text" name="didik2" placeholder="Anggota Keluarga 1..."></p>
                <p><input type="text" name="didik3" placeholder="Anggota Keluarga 2..."></p>
                <p><input type="text" name="didik4" placeholder="Anggota Keluarga 3..."></p>
                <p><input type="text" name="didik5" placeholder="Anggota Keluarga 4..."></p>
                <p><input type="text" name="didik6" placeholder="Anggota Keluarga 5..."></p></td>
            <td><p><label>Pekerjaan : </label></p>
                <p><input type="text" name="kerja1" placeholder="Kepala Keluarga..."></p>
                <p><input type="text" name="kerja2" placeholder="Anggota Keluarga 1..."></p>
                <p><input type="text" name="kerja3" placeholder="Anggota Keluarga 2..."></p>
                <p><input type="text" name="kerja4" placeholder="Anggota Keluarga 3..."></p>
                <p><input type="text" name="kerja5" placeholder="Anggota Keluarga 4..."></p>
                <p><input type="text" name="kerja6" placeholder="Anggota Keluarga 5..."></p></td>
            <td><p><label>Status Perkawinan : </label></p>
                <p><input type="text" name="kawin1" placeholder="Kepala Keluarga..."></p>
                <p><input type="text" name="kawin2" placeholder="Anggota Keluarga 1..."></p>
                <p><input type="text" name="kawin3" placeholder="Anggota Keluarga 2..."></p>
                <p><input type="text" name="kawin4" placeholder="Anggota Keluarga 3..."></p>
                <p><input type="text" name="kawin5" placeholder="Anggota Keluarga 4..."></p>
                <p><input type="text" name="kawin6" placeholder="Anggota Keluarga 5..."></p></td>
            <td><p><label>Status Hubungan : </label></p>
                <p><input type="text" name="hubung1" placeholder="Kepala Keluarga..."></p>
                <p><input type="text" name="hubung2" placeholder="Anggota Keluarga 1..."></p>
                <p><input type="text" name="hubung3" placeholder="Anggota Keluarga 2..."></p>
                <p><input type="text" name="hubung4" placeholder="Anggota Keluarga 3..."></p>
                <p><input type="text" name="hubung5" placeholder="Anggota Keluarga 4..."></p>
                <p><input type="text" name="hubung6" placeholder="Anggota Keluarga 5..."></p></td>
        </tr>
    </table>
        <p>
            <input type="submit" name="buatKK" value="Buat Kartu Keluarga" />
        </p>
    </form>
<?php 
	echo "hello";
    if(isset($_POST['buatKK'])){
            $kepal = $_POST['kepal'];
            $angkel1 = $_POST['angkel1'];
            $angkel2 = $_POST['angkel2'];
            $angkel3 = $_POST['angkel3'];
            $angkel4 = $_POST['angkel4'];
            $angkel5 = $_POST['angkel5'];
            $nik1 = $_POST['nik1'];
            $nik2 = $_POST['nik2'];
            $nik3 = $_POST['nik3'];
            $nik4 = $_POST['nik4'];
            $nik5 = $_POST['nik5'];
            $nik6 = $_POST['nik6'];
            $jkkepal = $_POST['jkkepal'];
            $jkangkel1 = $_POST['jkangkel1'];
            $jkangkel2 = $_POST['jkangkel2'];
            $jkangkel3 = $_POST['jkangkel3'];
            $jkangkel4 = $_POST['jkangkel4'];
            $jkangkel5 = $_POST['jkangkel5'];
            $alamat = $_POST['alamat'];
            $tempat1 = $_POST['tempat1'];
            $tempat2 = $_POST['tempat2'];
            $tempat3 = $_POST['tempat3'];
            $tempat4 = $_POST['tempat4'];
            $tempat5 = $_POST['tempat5'];
            $tempat6 = $_POST['tempat6'];
            $tl1 = $_POST['tl1'];
            $tl2 = $_POST['tl2'];
            $tl3 = $_POST['tl3'];
            $tl4 = $_POST['tl4'];
            $tl5 = $_POST['tl5'];
            $tl6 = $_POST['tl6'];
            $agamakepal = $_POST['agamakepal'];
            $agamaangkel1 = $_POST['agamaangkel1'];
            $agamaangkel2 = $_POST['agamaangkel3'];
            $agamaangkel3 = $_POST['agamaangkel3'];
            $agamaangkel4 = $_POST['agamaangkel4'];
            $agamaangkel5 = $_POST['agamaangkel5'];
            $didik1 = $_POST['didik1'];
            $didik2 = $_POST['didik2'];
            $didik3 = $_POST['didik3'];
            $didik4 = $_POST['didik4'];
            $didik5 = $_POST['didik5'];
            $didik6 = $_POST['didik6'];
            $kerja1 = $_POST['kerja1'];
            $kerja2 = $_POST['kerja2'];
            $kerja3 = $_POST['kerja3'];
            $kerja4 = $_POST['kerja4'];
            $kerja5 = $_POST['kerja5'];
            $kerja6 = $_POST['kerja6'];
            $kawin1 = $_POST['kawin1'];
            $kawin2 = $_POST['kawin2'];
            $kawin3 = $_POST['kawin3'];
            $kawin4 = $_POST['kawin4'];
            $kawin5 = $_POST['kawin5'];
            $kawin6 = $_POST['kawin6'];
            $hubung1 = $_POST['hubung1'];
            $hubung2 = $_POST['hubung2'];
            $hubung3 = $_POST['hubung3'];
            $hubung4 = $_POST['hubung4'];
            $hubung5 = $_POST['hubung5'];
            $hubung6 = $_POST['hubung6'];
            echo "<legend>Hasil Kartu Keluarga</legend>";
            echo "<table border=5>"; 
                echo "<tr>";
                    echo "<td>";
                        echo "Nama Lengkap : "; echo "<br>";
                        echo "Kepala Keluarga : "; echo $kepal; echo "<br>";
                        echo "Anggota Keluarga 1 : "; echo $angkel1; echo "<br>";
                        echo "Anggota Keluarga 2 : "; echo $angkel2; echo "<br>";
                        echo "Anggota Keluarga 3 : "; echo $angkel3; echo "<br>";
                        echo "Anggota Keluarga 4 : "; echo $angkel4; echo "<br>";
                        echo "Anggota Keluarga 5 : "; echo $angkel5; echo "<br>";
                    echo "</td>";
                    echo "<td>";
                        echo "NIK : "; echo "<br>";
                        echo "Kepala Keluarga : "; echo $nik1; echo "<br>";
                        echo "Anggota Keluarga 1 : "; echo $nik2; echo "<br>";
                        echo "Anggota Keluarga 2 : "; echo $nik3; echo "<br>";
                        echo "Anggota Keluarga 3 : "; echo $nik4; echo "<br>";
                        echo "Anggota Keluarga 4 : "; echo $nik5; echo "<br>";
                        echo "Anggota Keluarga 5 : "; echo $nik6; echo "<br>";
                    echo "</td>"; 
                    echo "<td>";
                        echo "Jenis Kelamin : "; echo "<br>";
                        echo "Kepala Keluarga : "; echo $jkkepal; echo "<br>";
                        echo "Anggota Keluarga 1 : "; echo $jkangkel1; echo "<br>";
                        echo "Anggota Keluarga 2 : "; echo $jkangkel2; echo "<br>";
                        echo "Anggota Keluarga 3 : "; echo $jkangkel3; echo "<br>";
                        echo "Anggota Keluarga 4 : "; echo $jkangkel4; echo "<br>";
                        echo "Anggota Keluarga 5 : "; echo $jkangkel5; echo "<br>";
                    echo "</td>";
                    echo "<td>";
                        echo "Alamat : "; echo $alamat;
                        echo "<br>"; 
                    echo "</td>";
                    echo "<td>";
                        echo "Tempat Lahir : "; echo "<br>";
                        echo "Kepala Keluarga : "; echo $tempat1; echo "<br>";
                        echo "Anggota Keluarga 1 : "; echo $tempat2; echo "<br>";
                        echo "Anggota Keluarga 2 : "; echo $tempat3; echo "<br>";
                        echo "Anggota Keluarga 3 : "; echo $tempat4; echo "<br>";
                        echo "Anggota Keluarga 4 : "; echo $tempat5; echo "<br>";
                        echo "Anggota Keluarga 5 : "; echo $tempat6; echo "<br>";
                    echo "</td>";
                    echo "<td>";
                        echo "Tanggal Lahir : "; echo "<br>";
                        echo "Kepala Keluarga : "; echo $tl1; echo "<br>";
                        echo "Anggota Keluarga 1 : "; echo $tl2; echo "<br>";
                        echo "Anggota Keluarga 2 : "; echo $tl3; echo "<br>";
                        echo "Anggota Keluarga 3 : "; echo $tl4; echo "<br>";
                        echo "Anggota Keluarga 4 : "; echo $tl5; echo "<br>";
                        echo "Anggota Keluarga 5 : "; echo $tl6; echo "<br>";
                    echo "</td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td>";
                        echo "Agama : "; echo "<br>";
                        echo "Kepala Keluarga : "; echo $agamakepal; echo "<br>";
                        echo "Anggota Keluarga 1 : "; echo $agamaangkel1; echo "<br>";
                        echo "Anggota Keluarga 2 : "; echo $agamaangkel2; echo "<br>";
                        echo "Anggota Keluarga 3 : "; echo $agamaangkel3; echo "<br>";
                        echo "Anggota Keluarga 4 : "; echo $agamaangkel4; echo "<br>";
                        echo "Anggota Keluarga 5 : "; echo $agamaangkel5; echo "<br>";
                    echo "</td>";
                    echo "<td>";
                        echo "Pendidikan : ";
                        echo "Kepala Keluarga : "; echo $didik1; echo "<br>";
                        echo "Anggota Keluarga 1 : "; echo $didik2; echo "<br>";
                        echo "Anggota Keluarga 2 : "; echo $didik3; echo "<br>";
                        echo "Anggota Keluarga 3 : "; echo $didik4; echo "<br>";
                        echo "Anggota Keluarga 4 : "; echo $didik5; echo "<br>";
                        echo "Anggota Keluarga 5 ; "; echo $didik6; echo "<br>";
                    echo "</td>";
                    echo "<td>";
                        echo "Pekerjaan : "; echo "<br>";
                        echo "Kepala Keluarga : "; echo $kerja1; echo "<br>";
                        echo "Anggota Keluarga 1 : "; echo $kerja2; echo "<br>";
                        echo "Anggota Keluarga 2 : "; echo $kerja3; echo "<br>";
                        echo "Anggota Keluarga 3 : "; echo $kerja4; echo "<br>";
                        echo "Anggota Keluarga 4 : "; echo $kerja5; echo "<br>";
                        echo "Anggota Keluarga 5 : "; echo $kerja6; echo "<br>";
                    echo "</td>";
                    echo "<td>";
                        echo "Status Perkawinan : "; echo "<br>";
                        echo "Kepala Keluarga : "; echo $kawin1; echo "<br>";
                        echo "Anggota Keluarga 1 : "; echo $kawin2; echo "<br>";
                        echo "Anggota Keluarga 2 : "; echo $kawin3; echo "<br>";
                        echo "Anggota Keluarga 3 : "; echo $kawin4; echo "<br>";
                        echo "Anggota Keluarga 4 : "; echo $kawin5; echo "<br>";
                        echo "Anggota Keluarga 5 : "; echo $kawin6; echo "<br>";
                    echo "</td>";
                    echo "<td>";
                        echo "Status Hubungan : "; echo "<br>";
                        echo "Kepala Keluarga : "; echo $hubung1; echo "<br>";
                        echo "Anggota Keluarga 1 : "; echo $hubung2; echo "<br>";
                        echo "Anggota Keluarga 2 : "; echo $hubung3; echo "<br>";
                        echo "Anggota Keluarga 3 : "; echo $hubung4; echo "<br>";
                        echo "Anggota Keluarga 4 : "; echo $hubung5; echo "<br>";
                        echo "Anggota Keluarga 5 : "; echo $hubung6; echo "<br>";
                    echo "</td>";
                    echo "<td>";
                        echo "Silahkan Upload File : ";
                        echo "<form action=buatKTP.php method=post enctype=multipart/form-data>"; 
                            echo "Upload Buku Nikah/Akta Perkawinan : ";
                            echo "<input type=file name=fileup>";
                            echo "Upload Surat Pengantar RT/RW : ";
                            echo "<input type=file name=fileup>";
                        echo "</form>"; 
                    echo "</td>";
                echo "</tr>";
            echo "</table>";
    }
 ?>
 <h4>Silahkan klik di bawah untuk pembayaran</h4>
 <button><a href="pembayaran.php">PEMBAYARAN</a></button>
</body>
<center><footer>copyright</footer></center>
<center><footer>Muhammad Daffiano Rahmatullah</footer></center>
<center><footer>1900018081</footer></center>
<center><footer>est 2021</footer></center>
</html>
