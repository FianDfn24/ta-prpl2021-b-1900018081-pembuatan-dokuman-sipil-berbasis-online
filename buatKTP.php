<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style2.css">
    <title>Pembuatan KTP</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<script type="text/javascript">
    function runPopup(){
        if(window.confirm("SILAHKAN ISI DENGAN JUJUR DAN TELITI!!!!!")) {
            window.alert("SILAHKAN DIISI!!!");
        }
    };
</script>
<body class="badan">
    <nav class="navbar navbar-inverse">
    <div class="container-fluid">
     <div class="navbar-header">
    <a href="index.php" class="navbar-brand">Pembuatan Dokumen Sipil Online</a>
   </div>
   <ul class="nav navbar-nav">
    <li><a href="index.php">Home</a></li>
    <li><a href="cs.php">Customer Services</a></li>
   </ul>
  </div>
 </nav>
<button onclick="runPopup()">Click Me</button>
  <script>  
     if(window.confirm("SILAHKAN ISI DENGAN JUJUR DAN TELITI!!!!")) {
        window.alert("SILAHKAN DIISI!!!");
     }
  </script>
    <form action="buatKTP.php" method="POST">
        <fieldset>
        <legend>KTP</legend>
        <table border="2">
            <tr>
                <td><p><label>NIK:</label> <input type="number" name="nik" placeholder="NIK..." /></p></td>
            </tr>
            <tr>
                <td><p><label>Nama:</label><input type="text" name="nama" placeholder="Nama..." /></p></td>
            </tr>
            <tr>
                <td>><p><label>Tempat/Tgl Lahir:</label><input type="text" name="ttl" placeholder="Tempat/Tgl Lahir..." /></p></td>
            </tr>
            <tr>
                <td><p>
                    <label>Jenis kelamin:</label>
                    <label><input type="radio" name="kelamin" value="LAKI-LAKI" /> LAKI-LAKI</label>
                    <label><input type="radio" name="kelamin" value="PERREMPUAN" /> PEREMPUAN</label>
                </p></td>
            </tr>
            <tr><
                <td><p><label>Alamat : </label><input type="text" name="alamat" placeholder="Alamat..." /></p></td>
            </tr>
            <tr>
                <td><p><label>RT/RW : </label><input type="text" name="rtw" placeholder="RT/RW..." /></p></td>
            </tr>
            <tr>
                <td><p><label>Kel/Desa : </label><input type="text" name="keldes" placeholder="Kel/Desa..." /></p></td>
            </tr>
            <tr>
                <td><p><label>Kecamatan : </label><input type="text" name="kecamatan" placeholder="Kecamatan..." /></p></td>
            </tr>
            <tr>
                <td><p><label>Agama : </label>
                    <select name="agama">
                    <option value=""></option>       
                    <option value="islam">Islam</option>
                    <option value="kristen">Kristen</option>
                    <option value="Khatolik">Khatolik</option>
                    <option value="hindu">Hindu</option>
                    <option value="budha">Budha</option>
                    <option value="Kong Hu Cu">Kong Hu Cu</option>
                </select>
                </p></td>
            </tr>
            <tr>
                <td><p><label>Status Perkawinan : </label><input type="text" name="sp" placeholder="Status Perkawinan..." /></p></td>
            </tr>
            <tr>
                <td><p><label>Pekerjaan : </label><input type="text" name="kerja" placeholder="Pekerjaan..." /></p></td>
            </tr>
            <tr>
                <td><p><label>Kewarganegaraan : </label><input type="text" name="warga" placeholder="Kewarganegaraan..." /></p></td>
            </tr>
            <tr>
                <td><p><label>Berlaku Hingga : </label><input type="text" name="laku" placeholder="Berlaku Hingga..." /></p></td>
            </tr>
    </table>
        <p>
            <input type="submit" name="buatktp" value="Buat KTP" />
        </p>
        </fieldset>
    </form>
    <?php 
        if(isset($_POST['buatktp'])){
            $nik = $_POST['nik'];
            $nama = $_POST['nama'];
            $ttl = $_POST['ttl'];
            $kelamin = $_POST['kelamin'];
            $alamat = $_POST['alamat'];
            $rtw = $_POST['rtw'];
            $keldes = $_POST['keldes'];
            $kecamatan = $_POST['kecamatan'];
            $agama = $_POST['agama'];
            $sp = $_POST['sp'];
            $kerja = $_POST['kerja'];
            $warga = $_POST['warga'];
            $laku = $_POST['laku'];
            echo "<legend>Hasil KTP</legend>";
            echo "NIK Anda : "; echo $nik;
            echo "<br/>";
            echo "Nama Anda : "; echo $nama;
            echo "<br/>";
            echo "Tempat, Tanggal Lahir Anda : "; echo $ttl;
            echo "<br/>";
            echo "Jenis Kelamin Anda : "; echo $kelamin;
            echo "<br/>";
            echo "Alamat Anda : "; echo $alamat;
            echo "<br/>";
            echo "RT/RW Anda : "; echo $rtw;
            echo "<br/>";
            echo "Kelurahan/Desa Anda : "; echo $keldes;
            echo "<br/>";
            echo "Kecamatan Anda : "; echo $kecamatan;
            echo "<br/>";
            echo "Agama Anda : "; echo $agama;
            echo "<br/>";
            echo "Status Perkawinan Anda : "; echo $sp;
            echo "<br/>";
            echo "Pekerjaan Anda : "; echo $kerja;
            echo "<br/>";
            echo "Kewarganegaraan Anda : "; echo $warga;
            echo "<br/>";
            echo "Berlaku Hingga : "; echo $laku;
            echo "<br>";
            echo "Silahkan Upload File : ";
            echo "<form action=buatKTP.php method=post enctype=multipart/form-data>"; 
                echo "Upload Kartu Keluarga : ";
                echo "<input type=file name=fileup>";
                echo "Upload Surat Pengantar RT/RW : ";
                echo "<input type=file name=fileup>";
            echo "</form>";
    }
     ?>
    <h4>Silahkan klik di bawah untuk pembayaran</h4>
    <button><a href="pembayaran.php">PEMBAYARAN</a></button>
</body>
<center><footer>copyright</footer></center>
<center><footer>Muhammad Daffiano Rahmatullah</footer></center>
<center><footer>1900018081</footer></center>
<center><footer>est 2021</footer></center>

</html>
